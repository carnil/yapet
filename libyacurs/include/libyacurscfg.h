/* include/libyacurscfg.h.  Generated from libyacurscfg.h.in by configure.  */
// -*- mode: c++ -*-

// Macros in here are required to make headers work without having
// config.h installed.
//
// Definitions in here only reflect a subset of config.h.

/* Define to 1 if you want to disable wchar support. */
/* #undef DISABLE_WCHAR */

/* Define to 1 if translation of program messages to the user's native
   language is requested. */
#define ENABLE_NLS 1

/* Define to 1 if a SysV or X/Open compatible Curses library is present */
#define HAVE_CURSES 1

/* Define to 1 if library supports color (enhanced functions) */
#define HAVE_CURSES_COLOR 1

/* Define to 1 if library supports X/Open Enhanced functions */
#define HAVE_CURSES_ENHANCED 1

/* Define to 1 if <curses.h> is present */
/* #undef HAVE_CURSES_H */

/* Define to 1 if library supports certain obsolete features */
#define HAVE_CURSES_OBSOLETE 1

/* Define to 1 if the Ncurses library is present */
/* #undef HAVE_NCURSES */

/* Define to 1 if the NcursesW library is present */
#define HAVE_NCURSESW 1

/* Define to 1 if <ncursesw/curses.h> is present */
/* #undef HAVE_NCURSESW_CURSES_H */

/* Define to 1 if <ncursesw.h> is present */
/* #undef HAVE_NCURSESW_H */

/* Define to 1 if <ncurses/curses.h> is present */
/* #undef HAVE_NCURSES_CURSES_H */

/* Define to 1 if <ncurses.h> is present */
#define HAVE_NCURSES_H 1

/* Define to 1 if you have the <cwchar> header file. */
#define HAVE_CWCHAR 1

/* Define to 1 if you have the <cwctype> header file. */
#define HAVE_CWCTYPE 1

/* Define to 1 if you have the <locale.h> header file. */
#define HAVE_LOCALE_H 1

/* Set to 1 if waddnstr() doesn't use const char* */
/* #undef WADDNSTR_USE_CHAR */

/* Set to 1 if waddstr() doesn't use const char* */
/* #undef WADDSTR_USE_CHAR */

/* Set to 1 if mvwaddnstr() doesn't use const char* */
/* #undef MVWADDNSTR_USE_CHAR */

/* Set to 1 if mvwaddstr() doesn't use const char* */
/* #undef MVWADDSTR_USE_CHAR */

/* Set to 1 if keypad has a return value */
#define KEYPAD_RETURN_INT 1

/* Define to 1 if you have the `resizeterm' function. */
/* #undef HAVE_RESIZETERM */

/* Define to 1 if you have the `resize_term' function. */
#define HAVE_RESIZE_TERM 1
